const { Router } = require('express');
const UserService = require('../services/userService');
const {
    createUserValid,
    updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// GET /api/users
router.get(
    '/',
    (req, res, next) => {
        try {
            const data = UserService.getAllUsers();

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

// GET /api/users/:id
router.get(
    '/:id',
    (req, res, next) => {
        try {
            const userIdObject = { id: req.params.id };
            const data = UserService.searchUser(userIdObject);

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

// POST /api/users
router.post(
    '/',
    createUserValid,
    (req, res, next) => {
        try {
            const {
                firstName,
                lastName,
                email,
                phoneNumber,
                password,
            } = req.body;
            const data = UserService.createUser({
                firstName,
                lastName,
                email,
                phoneNumber,
                password,
            });

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(400);
        } finally {
            next();
        }
    },
    responseMiddleware
);

// PUT /api/users/:id
router.put(
    '/:id',
    updateUserValid,
    (req, res, next) => {
        try {
            const userId = req.params.id;
            const dataToUpdate = req.body;
            const data = UserService.updateUser(userId, dataToUpdate);

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(400);
        } finally {
            next();
        }
    },
    responseMiddleware
);

// DELETE /api/users/:id
router.delete(
    '/:id',
    (req, res, next) => {
        try {
            const userId = req.params.id;
            const data = UserService.deleteUser(userId);

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;
