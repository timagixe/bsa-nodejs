const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
    createFighterValid,
    updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get(
    '/',
    (req, res, next) => {
        try {
            const data = FighterService.getAllFighters();

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.get(
    '/:id',
    (req, res, next) => {
        try {
            const fighterIdObject = { id: req.params.id };
            const data = FighterService.searchFighter(fighterIdObject);

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.post(
    '/',
    createFighterValid,
    (req, res, next) => {
        try {
            const { name, health, power, defense } = req.body;

            const data = FighterService.createFighter({
                name,
                health,
                power,
                defense,
            });

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(400);
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.put(
    '/:id',
    updateFighterValid,
    (req, res, next) => {
        try {
            const fighterId = req.params.id;
            const dataToUpdate = req.body;
            const data = FighterService.updateFighter(fighterId, dataToUpdate);

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(400);
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.delete(
    '/:id',
    (req, res, next) => {
        try {
            const fighterId = req.params.id;
            const data = FighterService.deleteFighter(fighterId);

            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;
