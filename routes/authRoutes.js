const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
    '/login',
    (req, res, next) => {
        try {
            // TODO: Implement login action
            const data = AuthService.login({
                email: req.body.email,
                password: req.body.password,
            });
            console.log(data);
            res.data = data;
            res.status(200);
        } catch (err) {
            res.err = err;
            res.status(404);
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;
