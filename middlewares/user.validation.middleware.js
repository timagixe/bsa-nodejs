const { user } = require('../models/user');

const regExpName = /^([a-zA-Z]+){2,}$/;
const regExpEmail = /^[0-9a-zA-Z][0-9a-zA-Z_\.]*[0-9a-zA-Z]@gmail\.com$/;
const regExpNumber = /^\+380[0-9]{9}$/;
const regExpPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/;

const isValid = (value, regExp) => {
    return value === undefined ? undefined : regExp.test(value);
};

const allValuesAreTrue = (...values) => {
    return values.every((value) => value === true);
};

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const { firstName, lastName, email, phoneNumber, password } = req.body;

    const firstNameIsValid = isValid(firstName, regExpName);
    const lastNameIsValid = isValid(lastName, regExpName);
    const emailIsValid = isValid(email, regExpEmail);
    const phoneNumberIsValid = isValid(phoneNumber, regExpNumber);
    const passwordIsValid = isValid(password, regExpPassword);

    console.log(
        firstNameIsValid,
        lastNameIsValid,
        emailIsValid,
        phoneNumberIsValid,
        passwordIsValid
    );

    if (
        allValuesAreTrue(
            firstNameIsValid,
            lastNameIsValid,
            emailIsValid,
            phoneNumberIsValid,
            passwordIsValid
        )
    ) {
        next();
    } else {
        res.status(400).json({
            error: true,
            message: 'User entity to create is not valid',
        });
    }
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const input = { ...req.body };
    const inputTrueAndFalseValues = [];

    // checks fields (according to models/user.js) validity
    for (let key of Object.keys(req.body)) {
        switch (key) {
            case 'firstName':
                isValid(input[key], regExpName)
                    ? inputTrueAndFalseValues.push(true)
                    : inputTrueAndFalseValues.push(false);
                break;
            case 'lastName':
                isValid(input[key], regExpName)
                    ? inputTrueAndFalseValues.push(true)
                    : inputTrueAndFalseValues.push(false);
                break;
            case 'email':
                isValid(input[key], regExpEmail)
                    ? inputTrueAndFalseValues.push(true)
                    : inputTrueAndFalseValues.push(false);
                break;
            case 'phoneNumber':
                isValid(input[key], regExpNumber)
                    ? inputTrueAndFalseValues.push(true)
                    : inputTrueAndFalseValues.push(false);
                break;
            case 'password':
                isValid(input[key], regExpPassword)
                    ? inputTrueAndFalseValues.push(true)
                    : inputTrueAndFalseValues.push(false);
                break;
        }
    }

    if (allValuesAreTrue(...inputTrueAndFalseValues)) {
        next();
    } else {
        res.status(400).json({
            error: true,
            message: 'User entity to update is not valid',
        });
    }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
