const { fighter } = require('../models/fighter');

const regExpName = /^([a-zA-Z]+){2,}$/;

const regExpHealth = /^[0-9a-zA-Z][0-9a-zA-Z_\.]*[0-9a-zA-Z]@gmail\.com$/;
const regExpPower = /^\+380[0-9]{9}$/;
const regExpDefense = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/;

const isValidName = (value, regExp) => {
    return value === undefined ? undefined : regExp.test(value);
};

const isValidNumber = (value, maxValue) => {
    return typeof Number(value) === 'number' && Number(value) <= maxValue;
};

const allValuesAreTrue = (...values) => {
    return values.every((value) => value === true);
};

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { name, health, power, defense } = req.body;

    const nameIsValid = isValidName(name, regExpName);
    const healthIsValid = isValidNumber(health, 100);
    const powerIsValid = isValidNumber(power, 100);
    const defenseIsValid = isValidNumber(defense, 10);

    // console.log(nameIsValid, healthIsValid, powerIsValid, defenseIsValid);

    if (
        allValuesAreTrue(
            nameIsValid,
            healthIsValid,
            powerIsValid,
            defenseIsValid
        )
    ) {
        next();
    } else {
        res.status(400).json({
            error: true,
            message: 'Fighter entity to create is not valid',
        });
    }
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const input = { ...req.body };
    const inputTrueAndFalseValues = [];

    // checks fields (according to models/user.js) validity
    for (let key of Object.keys(req.body)) {
        switch (key) {
            case 'name':
                inputTrueAndFalseValues.push(
                    isValidName(input[key], regExpName)
                );
                break;
            case 'health':
                inputTrueAndFalseValues.push(isValidNumber(input[key], 100));
                break;
            case 'power':
                inputTrueAndFalseValues.push(isValidNumber(input[key], 100));
                break;
            case 'defense':
                inputTrueAndFalseValues.push(isValidNumber(input[key], 10));
                break;
        }
    }

    if (allValuesAreTrue(...inputTrueAndFalseValues)) {
        next();
    } else {
        res.status(400).json({
            error: true,
            message: 'Fighter entity to update is not valid',
        });
    }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
