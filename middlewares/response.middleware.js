const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (!!res.err) {
        res.status(res.statusCode).json({
            error: true,
            message: res.err.message,
        });
    } else {
        res.status(res.statusCode).json(res.data);
    }
};

exports.responseMiddleware = responseMiddleware;
