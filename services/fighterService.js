const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAllFighters() {
        const allFighters = FighterRepository.getAll();

        // checks if there are fighters in db
        if (allFighters.length === 0) {
            throw Error('Fighters not found');
        }

        return allFighters;
    }

    searchFighter(search) {
        const fighter = FighterRepository.getOne(search);

        // checks if there is fighter with search params
        if (!fighter) {
            throw Error('Fighter not found');
        }

        return fighter;
    }

    createFighter(input) {
        // checks if there is fighter with such name
        if (!!FighterRepository.getOne({ name: input.name })) {
            throw Error('There is such fighter already');
        }

        return FighterRepository.create(input);
    }

    updateFighter(id, input) {
        // checks if there is fighter with such ID in DB
        if (!this.searchFighter({ id })) {
            throw Error('Fighter not found');
        }

        const newInput = {};

        // fullfills newInput with only fields according to the model/fighter.js
        for (let key of Object.keys(input)) {
            switch (key) {
                case 'name':
                    newInput[key] = input[key];
                    break;
                case 'health':
                    newInput[key] = input[key];
                    break;
                case 'power':
                    newInput[key] = input[key];
                    break;
                case 'defense':
                    newInput[key] = input[key];
                    break;
            }
        }

        return FighterRepository.update(id, newInput);
    }

    deleteFighter(id) {
        // if there is no fighter with such id it will throw an error
        this.searchFighter({ id });

        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();
