const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user

    getAllUsers() {
        const allUsers = UserRepository.getAll();

        // checks if there are users in db
        if (allUsers.length === 0) {
            throw Error('Users not found');
        }

        return allUsers;
    }

    searchUser(search) {
        const user = UserRepository.getOne(search);

        // checks if there is user with search params
        if (!user) {
            throw Error('User not found');
        }

        return user;
    }

    createUser(input) {
        // checks if there are users with such email
        if (
            !!UserRepository.getOne({
                email: input.email,
            }) ||
            !!UserRepository.getOne({
                phoneNumber: input.phoneNumber,
            })
        ) {
            console.log(123123);
            throw Error('There is such user already');
        }

        return UserRepository.create(input);
    }

    updateUser(id, input) {
        // checks if there is user with such ID in DB
        if (!this.searchUser({ id })) {
            throw Error('No user with such id');
        }

        const newInput = {};

        // fullfills newInput with only fields according to the model/user.js
        for (let key of Object.keys(input)) {
            switch (key) {
                case 'firstName':
                    newInput[key] = input[key];
                    break;
                case 'lastName':
                    newInput[key] = input[key];
                    break;
                case 'email':
                    newInput[key] = input[key];
                    break;
                case 'phoneNumber':
                    newInput[key] = input[key];
                    break;
                case 'password':
                    newInput[key] = input[key];
                    break;
            }
        }

        return UserRepository.update(id, newInput);
    }

    deleteUser(id) {
        // if there is no user with such id it will throw an error
        this.searchUser({ id });

        return UserRepository.delete(id);
    }
}

module.exports = new UserService();
